function out = drawObservations_xz(pose, observations, offset)
	display("drawing observations...");
	N = length(observations.observation);
	l = 1.5;
	

	mu_x = pose(1);
	mu_y = pose(2);
	mu_z = pose(3);
	mu_phi = pose(4);
	mu_theta = pose(5);
	mu_psi = pose(6);

	mu_t = pose(1:3,:);
	mu_r = pose(4:6,:);

	%Body 3-2-1
	Rx = [1 0 0; 0 cos(mu_phi) -sin(mu_phi); 0 sin(mu_phi) cos(mu_phi)];
	Ry = [cos(mu_theta) 0 sin(mu_theta); 0 1 0; -sin(mu_theta) 0 cos(mu_theta)];
	Rz = [cos(mu_psi) -sin(mu_psi) 0; sin(mu_psi) cos(mu_psi) 0; 0 0 1];
	dRx = Rz*Ry*[0 0 0; 0 -sin(mu_phi) -cos(mu_phi); 0 cos(mu_phi) -sin(mu_phi)];
	dRy = Rz*[-sin(mu_theta) 0 cos(mu_theta); 0 0 0; -cos(mu_theta) 0 -sin(mu_theta)]*Rx;
	dRz = [-sin(mu_psi) -cos(mu_psi) 0; cos(mu_psi) -sin(mu_psi) 0; 0 0 0]*Ry*Rx;
	R = Rz*Ry*Rx;

	# sensor offset
	offset_x = offset.x;
	offset_y = offset.y;
	offset_z = offset.z;
	offset_phi = offset.phi;
	offset_theta = offset.theta;
	offset_psi = offset.psi;

	%Body 3-2-1
	Rsx = [1 0 0; 0 cos(offset_phi) -sin(offset_phi); 0 sin(offset_phi) cos(offset_phi)];
	Rsy = [cos(offset_theta) 0 sin(offset_theta); 0 1 0; -sin(offset_theta) 0 cos(offset_theta)];
	Rsz = [cos(offset_psi) -sin(offset_psi) 0; sin(offset_psi) cos(offset_psi) 0; 0 0 1];
	Rs = Rsz*Rsy*Rsx;
	sens_xyz = [offset_x; offset_y; offset_z];



	if N > 0
	
	  #determine highlight mode
		mode = 'points';
		if isfield(observations.observation(1),'bearing')
			mode = 'bearing';
		end
		
		hold on;
		
		#for each observation
		for i=1:N
			current_observation = observations.observation(i);
			
			#default: points
			if strcmp(mode, 'points')
			
			  #draw points - with pointers towards them
#				land_obs = [current_observation.x_pose; current_observation.z_pose; 0];		
#				land_abs_pose = t2v(v2t(pose)*v2t(land_obs));
#				current_observation.x_pose = land_abs_pose(1);
#				current_observation.z_pose = land_abs_pose(2);	
#        drawShape('circle', [land_abs_pose(1), land_abs_pose(2), 0.11], 'fill', 'b');
	meas_xyz = [current_observation.x_pose; current_observation.y_pose; current_observation.z_pose];
			landmark_position_in_robot = Rs * meas_xyz + sens_xyz;
			land_obs = mu_t + R*landmark_position_in_robot;
	drawShape('circle', [land_obs(1), land_obs(3), 0.11], 'fill', 'b');
			else
			
			  #draw measured bearing
				land_obs = current_observation.bearing;
				incr_y = sin(land_obs)*l;
				incr_x = cos(land_obs)*l;
				ray_x = pose(1) + incr_x*cos(pose(3)) - incr_y*sin(pose(3));
				ray_y = pose(2) + incr_y*cos(pose(3)) + incr_x*sin(pose(3));
				plot([pose(1) ray_x] , [pose(2) ray_y], 'b', 'linewidth', 1.5);
			endif
		endfor		
	endif
display("drawing observations is done!");
endfunction

