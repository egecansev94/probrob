function out = plotState_xz(varargin)
display("plot state...");
  landmarks    = varargin{1};
  pose         = varargin{2};
  covariance   = varargin{3};
  observations = varargin{4};
  offset       = varargin{7};

  #check if we have additional input
  if (nargin > 4)
    trajectory = varargin{5};
    real_trajectory = varargin{6}; 
 endif

	%robot_pose = pose(1:3);
#	robot_pose = [pose(1); pose(3); pose(5)];
	robot_pose = [pose(1); pose(2); pose(3); pose(4); pose(5); pose(6)];
	state_dim  = size(pose,1);
	map_size   = (state_dim - 6)/3;	
	%map_size   = (state_dim - 3)/2; #number of landmarks in the state (if any)

	if(nargin == 2) %init step
    hold off;
		drawLandmarks_xz(landmarks);
		drawRobot(robot_pose);

	else
		hold off;

    #draw robot trajectory
    if (nargin > 4)
      drawTrajectory_xz(trajectory,'k');
      drawTrajectory_xz(real_trajectory,'m');
    endif

    #highlight current observations
    drawObservations_xz(robot_pose, observations, offset);
	robot_pose = [pose(1); pose(3); pose(5)];
    #draw landmarks
		drawLandmarks_xz(landmarks);

		#plot robot covariance		
		plotcov2d(robot_pose(1),robot_pose(2),[covariance(1,1), covariance(1,3);
						       covariance(3,1), covariance(3,3)],'k', 7.5);
		display(map_size);

#		plot landmark covariances (if any)
		for i=7:3:(3*map_size+4)
		  plotcov2d(pose(i,1), pose(i+2,1), [covariance(i,i), covariance(i,i+2);
						     covariance(i+2,i), covariance(i+2,i+2)],'r',2);
		endfor

    #draw robot pose
    drawRobot(robot_pose);

	endif
  %axis([-20, 20, -20, 20]);
	drawnow;
display("plot state done!");
endfunction

