% #   This source code is part of the localization and SLAM package
% #   deveoped for the lectures of probabilistic robotics at 
% #   Sapienza, University of Rome.
% #  
% #     Copyright (c) 2016 Bartolomeo Della Corte, Giorgio Grisetti
% #  
% #   It is licences under the Common Creative License,
% #   Attribution-NonCommercial-ShareAlike 3.0
% #  
% #   You are free:
% #     - to Share - to copy, distribute and transmit the work
% #     - to Remix - to adapt the work
% #  
% #   Under the following conditions:
% #  
% #     - Attribution. You must attribute the work in the manner specified
% #       by the author or licensor (but not in any way that suggests that
% #       they endorse you or your use of the work).
% #    
% #     - Noncommercial. You may not use this work for commercial purposes.
% #    
% #     - Share Alike. If you alter, transform, or build upon this work,
% #       you may distribute the resulting work only under the same or
% #       similar license to this one.
% #  
% #   Any of the above conditions can be waived if you get permission
% #   from the copyright holder.  Nothing in this license impairs or
% #   restricts the author's moral rights.
% #  
% #   This software is distributed in the hope that it will be useful,
% #   but WITHOUT ANY WARRANTY; without even the implied 
% #   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
% #   PURPOSE.
% #
%   
% # load a file.g2o file and returns the four structs of landmark, poses, transitions, observations

function [landmarks, poses, transitions, observations, offsets, no_obs_id] = loadG2o_project(filepath)

	%%-----G2O specification---
	PARAMS_SE3OFFSET = 'PARAMS_SE3OFFSET';
	VERTEX_TRACKXYZ = 'VERTEX_TRACKXYZ';
	VERTEX_SE3_QUAT = 'VERTEX_SE3:QUAT';
	EDGE_SE3_PRIOR = 'EDGE_SE3_PRIOR';
	EDGE_SE3_TRACKXYZ = 'EDGE_SE3_TRACKXYZ';
	%%-------------------------

	%open the file
	fid = fopen(filepath, 'r');
	

	%debug stuff
	i_vert_xyz = 0;
	i_vert_se3 = 0;
	i_edge_se3 = 0;
	i_edge_se3_xyz = 0;
  
	%    
	curr_id = -1;
	
	while true
		%get current line
		c_line = fgetl(fid);

		%stop if EOF
		if c_line == -1
			break;
		end

		%Split the line using space as separator
		elements = strsplit(c_line,' ');

		switch(elements{1})
			case PARAMS_SE3OFFSET
				offsets(end+1) = extractOffset(elements);
			case VERTEX_TRACKXYZ
        			landmarks(end+1) = extractLandmark(elements);
				i_vert_xyz = i_vert_xyz + 1; %do not use pre/post increment. Keep the Matlab compatibility
			case VERTEX_SE3_QUAT
        			poses(end+1) = extractPose(elements);
				i_vert_se3 = i_vert_se3 + 1;
			case EDGE_SE3_PRIOR
		        	transitions(end+1) = extractTransition(elements);
				i_edge_se3 = i_edge_se3 + 1;
				if flag_state == 0				
					no_obs_id(end+1) = transitions(end).id_from;
				end
				flag_state=0;
			case EDGE_SE3_TRACKXYZ
				flag_state=1;
				current_obs = extractPoint(elements);
				if current_obs.pose_id == curr_id
					observations(end).observation(end+1) = current_obs.observation;
				else
				        observations(end+1) = current_obs;
					curr_id = observations(end).pose_id;
					i_edge_se3_xyz = i_edge_se3_xyz + 1;
					prev=curr_id;

				end

			otherwise
				disp('Error in reading first element');
		end
	end
  
  printf('[G2oWrapper] loading file...\n#landmarks: %d \n#poses: %d \n',i_vert_xyz, i_vert_se3);
  printf('#transitions: %d \n',i_edge_se3);
  printf('#observation(point): %d \n',i_edge_se3_xyz);  
  fflush(stdout);
	printf(" %d \n", observations(1).observation.id);
end

function out = extractLandmark(elements)
  id = str2double(elements{2});
  x_pose = str2double(elements{3});
  y_pose = str2double(elements{4});
  z_pose = str2double(elements{5});
  out = landmark(id,[x_pose,y_pose,z_pose]);
end

function out = extractPose(elements)
  id = str2double(elements{2});

  x_pose = str2double(elements{3});
  y_pose = str2double(elements{4});
  z_pose = str2double(elements{5});

  qx_pose = str2double(elements{6});
  qy_pose = str2double(elements{7});
  qz_pose = str2double(elements{8});
  qw_pose = str2double(elements{9});
  [phi_pose, theta_pose, psi_pose] = q2e(qx_pose, qy_pose, qz_pose, qw_pose);
  #display([x_pose, y_pose, z_pose, phi_pose, theta_pose, psi_pose]);
  out = pose_project(id,x_pose, y_pose, z_pose, phi_pose, theta_pose, psi_pose);
end

function out = extractTransition(elements)
  from_id = str2double(elements{2});
  sensor_id = str2double(elements{3});
  x_t = str2double(elements{4});
  y_t = str2double(elements{5});
  z_t = str2double(elements{6});
  
  qx_t = str2double(elements{7});
  qy_t = str2double(elements{8});
  qz_t = str2double(elements{9});
  qw_t = str2double(elements{10});  
  [phi_t, theta_t, psi_t] = q2e(qx_t, qy_t, qz_t, qw_t);

  info_vect = zeros(1,21);
  for i =  11:columns(elements)-1
	info_vect(i-10) = str2double(elements{i});
  end
  info_mat = uptri(info_vect);

  out = transition_project(from_id,sensor_id, [x_t;y_t;z_t;phi_t;theta_t;psi_t], info_mat);
end

function out = extractPoint(elements)
  from_id = str2double(elements{2});
  land_id = str2double(elements{3});
  sensor_id = str2double(elements{4});
  x_p = str2double(elements{5});
  y_p = str2double(elements{6});
  z_p = str2double(elements{7});

  info_vect = zeros(1,6);
  for i =  8:columns(elements)-1
	info_vect(i-7) = str2double(elements{i});
  end
  info_mat = uptri(info_vect);

  out = observation_project(from_id, land_id, sensor_id, [x_p; y_p; z_p], info_mat);
end

function out = extractOffset(elements)
  sensor_id = str2double(elements{2});
  x_s = str2double(elements{3});
  y_s = str2double(elements{4});
  z_s = str2double(elements{5});

  qx_s = str2double(elements{6});
  qy_s = str2double(elements{7});
  qz_s = str2double(elements{8});
  qw_s = str2double(elements{9}); 
  [phi_s, theta_s, psi_s] = q2e(qx_s, qy_s, qz_s, qw_s);

  out = pose_project(sensor_id ,x_s, y_s, z_s, phi_s, theta_s, psi_s);
end

# Taken from https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles

function [phi, theta,psy] = q2e(q_x, q_y, q_z, q_w)
	# roll (x-axis rotation)
	sinr_cosp = +2.0 * (q_w * q_x + q_y * q_z);
	cosr_cosp = +1.0 - 2.0 * (q_x * q_x + q_y * q_y);
	phi = atan2(sinr_cosp, cosr_cosp);

	# pitch (y-axis rotation)
	sinp = +2.0 * (q_w * q_y - q_z * q_x);

	if abs(sinp) >= 1
		
		theta = copysign(M_PI / 2, sinp); # use 90 degrees if out of range
	else
		theta = asin(sinp);
	end
	# yaw (z-axis rotation)
	siny_cosp = +2.0 * (q_w * q_z + q_x * q_y);
	cosy_cosp = +1.0 - 2.0 * (q_y * q_y + q_z * q_z);  
	psy = atan2(siny_cosp, cosy_cosp);
end

function matrix = uptri(elements)
	len = columns(elements);
	summ = 0;
	dummy =	0;
	while summ < len
		dummy = dummy + 1;
		summ = summ + dummy;
	end	
	if summ == len
		dim = dummy;
	else
		disp('Error in numbers of elements in information matrix');
		return;
	end	
	matrix = zeros(dim);
	indx=1;
	j=1;
	for i = 1:dim
		matrix(i,j:dim) = elements(1,indx:indx+dim-j);
		indx = indx + dim - j + 1;
		j = j+1;
	end
end

function out = hole(id_from)
  from_id = id_from;
  land_id = -1;
  sensor_id = -1;
  x_p = 0;
  y_p = 0;
  z_p = 0;

  info_vect = zeros(1,6);
  info_mat = uptri(info_vect);
  out = observation_project(from_id, land_id, sensor_id, [x_p; y_p; z_p], info_mat);
end
