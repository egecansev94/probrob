% compute the measurement function for a point based observation
% 
% in case of a differential drive robot, ignore uy (the case seen in the classroom)
% inputs:
%   state_dim: total state dimension (robot + landmark)
%   mu_robot: current robot pose estimate (x,y,theta)
%   absolute_landmark_position: current landmark position in world coordinate 
%   landmark_state_vector_index: index of the landmark in the mu vector
%
% outputs:
%   measurement_prediction: prediction of the landmark in mu_robot frame
%   C_m: Jacobian wrt robot and wrt landmark for kalman filter correction

function [measurement_prediction, C_m] = measurement_function_project(state_dim, mu_robot, absolute_landmark_position, landmark_state_vector_index)

	robot_t = mu_robot(1:2);
	theta   = mu_robot(6);

	c=cos(theta);
	s=sin(theta);

	Rt=[c,s;-s c];    #transposed rotation matrix
	Rtp=[-s,c;-c,-s]; #derivative of transposed rotation matrix	

	%where I predict i will see that landmark
	delta_t = absolute_landmark_position(1:2) - robot_t;
	measurement_prediction = Rt* delta_t;
	
	%init Jacobian
	C_m = zeros(2, state_dim);

	%Jacobian w.r.t robot
	C_m(1:2,1:2)=-Rt;
	C_m(1:2,3)=Rtp*delta_t;

	%Jacobian w.r.t landmark
	C_m(:,landmark_state_vector_index:landmark_state_vector_index+1)=Rt;
	display('measurement is done');
	#pause();
#	mu_x = mu_robot(1);
#	mu_y = mu_robot(2);
#	mu_z = mu_robot(3);
#	mu_phi = mu_robot(4);
#	mu_theta = mu_robot(5);
#	mu_psi = mu_robot(6);
#	
#	mu_t = mu_robot(1:3,:);
#	mu_r = mu_robot(4:6,:);

#	%Body 3-2-1
#	Rx = [1 0 0; 0 cos(mu_phi) -sin(mu_phi); 0 sin(mu_phi) cos(mu_phi)];
#	Ry = [cos(mu_theta) 0 sin(mu_theta); 0 1 0; -sin(mu_theta) 0 cos(mu_theta)];
#	Rz = [cos(mu_psi) -sin(mu_psi) 0; sin(mu_psi) cos(mu_psi) 0; 0 0 1];
#	dRx = Rz*Ry*[0 0 0; 0 -sin(mu_phi) -cos(mu_phi); 0 cos(mu_phi) -sin(mu_phi)];
#	dRy = Rz*[-sin(mu_theta) 0 cos(mu_theta); 0 0 0; -cos(mu_theta) 0 -sin(mu_theta)]*Rx;
#	dRz = [-sin(mu_psi) -cos(mu_psi) 0; cos(mu_psi) -sin(mu_psi) 0; 0 0 0]*Ry*Rx;
#	Rt = transpose(Rz*Ry*Rx);
#	%Rt = transpose(Rx*Ry*Rz);

#	

#	measurement_prediction = Rt*(absolute_landmark_position-mu_t);



#	%jacobian piece w.r.t. robot
#	C_m = zeros(3, state_dim);
#	C_m(1:3,1:3) = -Rt;
#	C_m(1:3,4) = dRx*(absolute_landmark_position-mu_t);
#	C_m(1:3,5) = dRy*(absolute_landmark_position-mu_t);
#	C_m(1:3,6) = dRz*(absolute_landmark_position-mu_t);

#	#jacobian piece w.r.t. landmark
#	C_m(:,landmark_state_vector_index:landmark_state_vector_index+2) = Rt;
	

end
