#this function adds new landmarks to the current state and current sigma
#inputs:
#  mu: mean, 
#  sigma: covariance of the robot-landmark set (x,y, theta, l_1, ..., l_N)
#
#  measurements:
#            a structure containing n measurements of landmarks
#            for each measurement we have
#            - the index of the landmark seen
#            - the location where we have seen the landmark (x,y) w.r.t the robot
#
#  id_to_state_map:
#            mapping that given the id of the measurement, returns its position in the mu vector
#  state_to_id_map:
#            mapping that given the index of mu vector, returns the id of the respective landmark
#
#outputs:
#  [mu, sigma]: the updated mean and covariance with new landmarks
#  [id_to_state_map, state_to_id_map]: the updated mapping vector between landmark position in mu vector and its id

function [mu, sigma, id_to_state_map, state_to_id_map, last_landmark_id] = addNewLandmarks(mu, sigma, measurements, offset, id_to_state_map, state_to_id_map, last_landmark_id)
display("add new landmarks...");
display("size of mu:");
display(size(mu));
	# we precompute some quantities that come in handy later on
	mu_x = mu(1);
	mu_y = mu(2);
	mu_z = mu(3);
	mu_phi = mu(4);
	mu_theta = mu(5);
	mu_psi = mu(6);

	mu_t = mu(1:3,:);
	mu_r = mu(4:6,:);
	
	%Body 3-2-1
	Rx = [1 0 0; 0 cos(mu_phi) -sin(mu_phi); 0 sin(mu_phi) cos(mu_phi)];
	Ry = [cos(mu_theta) 0 sin(mu_theta); 0 1 0; -sin(mu_theta) 0 cos(mu_theta)];
	Rz = [cos(mu_psi) -sin(mu_psi) 0; sin(mu_psi) cos(mu_psi) 0; 0 0 1];
	dRx = Rz*Ry*[0 0 0; 0 -sin(mu_phi) -cos(mu_phi); 0 cos(mu_phi) -sin(mu_phi)];
	dRy = Rz*[-sin(mu_theta) 0 cos(mu_theta); 0 0 0; -cos(mu_theta) 0 -sin(mu_theta)]*Rx;
	dRz = [-sin(mu_psi) -cos(mu_psi) 0; cos(mu_psi) -sin(mu_psi) 0; 0 0 0]*Ry*Rx;
#	dRx = [0 0 0; 0 -sin(mu_phi) -cos(mu_phi); 0 cos(mu_phi) -sin(mu_phi)]*Ry*Rz;
#	dRy = Rx*[-sin(mu_theta) 0 cos(mu_theta); 0 0 0; -cos(mu_theta) 0 -sin(mu_theta)]*Rz;
#	dRz = Rx*Ry*[-sin(mu_psi) -cos(mu_psi) 0; cos(mu_psi) -sin(mu_psi) 0; 0 0 0];
	#R = Rx*Ry*Rz;	
	R = Rz*Ry*Rx;


	# sensor offset
	offset_x = offset.x;
	offset_y = offset.y;
	offset_z = offset.z;
	offset_phi = offset.phi;
	offset_theta = offset.theta;
	offset_psi = offset.psi;

	%Body 3-2-1
	Rsx = [1 0 0; 0 cos(offset_phi) -sin(offset_phi); 0 sin(offset_phi) cos(offset_phi)];
	Rsy = [cos(offset_theta) 0 sin(offset_theta); 0 1 0; -sin(offset_theta) 0 cos(offset_theta)];
	Rsz = [cos(offset_psi) -sin(offset_psi) 0; sin(offset_psi) cos(offset_psi) 0; 0 0 1];
	#Rs = Rsx*Rsy*Rsz;
	Rs = Rsz*Rsy*Rsx;
	sens_xyz = [offset_x; offset_y; offset_z];

	#landmarks
	M = length(measurements.observation); #number of measured landmarks
	%n = (rows(mu)-3)/2;                   #current number of landmarks in the state
	n = (rows(mu)-6)/3;

	#Now its time to add, if observed, the NEW landmaks, without applying any correction
	for i=1:M

		#retrieve info about the observed landmark
		measurement = measurements.observation(i);
#		display(measurement.id)
		#if the measurement.id == -1 it means that the data association step failed for this measurement
    		if (measurement.id == 0)

		    #we could not associate the measurement to a landmark so we cannot use the measurement
	  	  	continue;

    		elseif(measurement.id == -1)

		    #we have found a new landmark
	    		measurement.id = last_landmark_id++;
    		endif

		#fetch the position in the state vector corresponding to the actual measurement
		state_pos_of_landmark = id_to_state_map(measurement.id);

		#IF current landmark is a NEW landmark
		if(state_pos_of_landmark == -1) 

			#adjust direct and reverse id mappings
			n++;
			id_to_state_map(measurement.id) = n;
			state_to_id_map(n)              = measurement.id;

			#compute landmark position in the world
			meas_xyz = [measurement.x_pose; measurement.y_pose; measurement.z_pose];
			landmark_position_in_robot = Rs * meas_xyz + sens_xyz;
#			display('abs pose wrt robot:');
#			display(landmark_position_in_robot);
			#landmark_position_in_robot = meas_xyz;
			landmark_position_in_world = mu_t + R*landmark_position_in_robot;
#			display('abs pose wrt world:');
#			display(landmark_position_in_world);
			#retrieve from the index the position of the landmark block in the state
			id_state = 7+3*(n-1);

			#adding the landmark state to the full state
			mu(id_state:id_state+2,1) = landmark_position_in_world;
			%display(mu);
			#initial noise assigned to a new landmark
			#for simplicity we put a high value only in the diagonal.
			#A more deeper analysis on the initial noise should be made.
			initial_landmark_noise = 0.01;
			sigma_landmark         = eye(3)*initial_landmark_noise;
			%display("sizes:");
			%display(sigma_landmark);
			%display(size(sigma));
			#adding the landmark covariance to the full covariance
%display(id_state);
			sigma(id_state,:)   = 0;
			sigma(id_state+1,:) = 0;
			sigma(id_state+2,:) = 0;
			sigma(:,id_state)   = 0;
			sigma(:,id_state+1) = 0;
			sigma(:,id_state+2) = 0;
			%display(size(sigma));
			#set the covariance block
			sigma(id_state:id_state+2, id_state:id_state+2) = sigma_landmark;
			%display(sigma);
			printf("observed new landmark with identifier: %i \n", measurement.id);
			fflush(stdout);
		endif
	endfor

display("add new landmarks is done!");
endfunction

