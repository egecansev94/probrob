#this function computes the update (also called correction)
#step of the filter
#inputs:
#  mu: mean, 
#  sigma: covariance of the robot (x,y.theta)
#  landmarks: a structure of landmarks, we can fetch the
#            position of a landmark given its index
#            by using the tools in the library
#  observations:
#            a structure containing n observations of landmarks
#            for each observation we have
#            - the index of the landmark seen
#            - the location where we have seen the landmark (x,y) w.r.t the robot
#outputs:
#  [mu, sigma]: the updated mean and covariance

function [mu, sigma, id_to_state_map, state_to_id_map] = correction(mu, sigma, observations, offset, id_to_state_map, state_to_id_map)
	display("correction...");
	% determine how many landmarks we have seen
	num_landmarks_seen = length(observations.observation);
	% dimension of the state in dim, in our case is fixed to 3
	dimension_state = size(mu,1);	
	%if I've seen no landmarks, i do nothing
	if (num_landmarks_seen==0)
		return;
	endif

	# we precompute some quantities that come in handy later on
	mu_x = mu(1);
	mu_y = mu(2);
	mu_z = mu(3);
	mu_phi = mu(4);
	mu_theta = mu(5);
	mu_psi = mu(6);
	
	mu_t = mu(1:3,:);
	mu_r = mu(4:6,:);

	%Body 3-2-1
	Rx = [1 0 0; 0 cos(mu_phi) -sin(mu_phi); 0 sin(mu_phi) cos(mu_phi)];
	Ry = [cos(mu_theta) 0 sin(mu_theta); 0 1 0; -sin(mu_theta) 0 cos(mu_theta)];
	Rz = [cos(mu_psi) -sin(mu_psi) 0; sin(mu_psi) cos(mu_psi) 0; 0 0 1];
	dRx = Rz*Ry*[0 0 0; 0 -sin(mu_phi) -cos(mu_phi); 0 cos(mu_phi) -sin(mu_phi)];
	dRy = Rz*[-sin(mu_theta) 0 cos(mu_theta); 0 0 0; -cos(mu_theta) 0 -sin(mu_theta)]*Rx;
	dRz = [-sin(mu_psi) -cos(mu_psi) 0; cos(mu_psi) -sin(mu_psi) 0; 0 0 0]*Ry*Rx;
#	dRx = [0 0 0; 0 -sin(mu_phi) -cos(mu_phi); 0 cos(mu_phi) -sin(mu_phi)]*Ry*Rz;
#	dRy = Rx*[-sin(mu_theta) 0 cos(mu_theta); 0 0 0; -cos(mu_theta) 0 -sin(mu_theta)]*Rz;
#	dRz = Rx*Ry*[-sin(mu_psi) -cos(mu_psi) 0; cos(mu_psi) -sin(mu_psi) 0; 0 0 0];
	#R = Rx*Ry*Rz;	
	R = Rz*Ry*Rx;
	Rt = transpose(R);


	# sensor offset
	offset_x = offset.x;
	offset_y = offset.y;
	offset_z = offset.z;
	offset_phi = offset.phi;
	offset_theta = offset.theta;
	offset_psi = offset.psi;

	%Body 3-2-1
	Rsx = [1 0 0; 0 cos(offset_phi) -sin(offset_phi); 0 sin(offset_phi) cos(offset_phi)];
	Rsy = [cos(offset_theta) 0 sin(offset_theta); 0 1 0; -sin(offset_theta) 0 cos(offset_theta)];
	Rsz = [cos(offset_psi) -sin(offset_psi) 0; sin(offset_psi) cos(offset_psi) 0; 0 0 1];
	Rs = Rsz*Rsy*Rsx;
	#Rs = Rsx*Rsy*Rsz;
	Rst = transpose(Rs);
	sens_xyz = [offset_x; offset_y; offset_z];

	number_of_known_landmarks = 0;

	%  c=cos(mu_theta);
	%  s=sin(mu_theta);
	%  Rt=[c,s;-s c]; # transposed rotation matrix
	%  Rtp=[-s,c;-c,-s]; # derivative of transposed rotation matrix

	# here in one go, we go through all landmark measurements vector
	# for each landmark, we assemble
	# the "total" measurement vector, containing all stacked measures
	# the "total" prediction vector, containing all staked predictions
	# the "total" jacobian, consisting of all jacobians of predictions stacked
	# octave allows to "Add" rows to a matrix, thus we dynamically resize them

	for i=1:num_landmarks_seen
		%retrieve info about the observed landmark

		measurement = observations.observation(i);

		#if the measurement.id < 1 it means that the data association step failed or is in doubt for this measurement
  	        #we cannot use it for our correction - skip it
	        if (measurement.id < 1)
		        continue;
	        endif

		#fetch the position in the state vector corresponding to the actual measurement
		n = id_to_state_map(measurement.id);

		#IF current landmark is a REOBSERVED LANDMARK
		if(n != -1) 

			#compute the index (vector coordinate) in the state vector corresponding to the pose of the landmark;	
			%id_state = 4+2*(n-1);
			id_state = 7+3*(n-1);
			
			#increment the counter of observations originating
			#from already known landmarks
			number_of_known_landmarks++;

			meas_xyz = [measurement.x_pose; measurement.y_pose; measurement.z_pose];
			abs_meas = Rs * meas_xyz + sens_xyz;

			z_t(end+1,:) = abs_meas(1); % where we see the landmark
			z_t(end+1,:) = abs_meas(2);	% in frame of robot
			z_t(end+1,:) = abs_meas(3);

			#fetch the position of the landmark in the state (its x and y coordinate)
      			l=mu(id_state:id_state+2,:);
			%where I should see that landmark
			%l = [lx; ly; lz];
			t=[mu_x ; mu_y; mu_z];
			%	display(measurement);
			%	display(current_land);
			measure_prediction = Rt*(l-t);

			h_t(end+1,:) = measure_prediction(1);	%in frame of robot
			h_t(end+1,:) = measure_prediction(2);
			h_t(end+1,:) = measure_prediction(3);

			%jacobian piece w.r.t. robot
			C = zeros(3, dimension_state);
			C(1:3,1:3) = -Rt;
			C(1:3,4) = dRx*(l-t);
			C(1:3,5) = dRy*(l-t);
			C(1:3,6) = dRz*(l-t);

			#jacobian piece w.r.t. landmark
      			C(:,id_state:id_state+2) = Rt;
			%display(C);
			C_t(end+1,:) = C(1,:);
			C_t(end+1,:) = C(2,:);
			C_t(end+1,:) = C(3,:);
			%display(C_t);
		endif
	endfor

	#if I have seen again at least one landmark
  	#I need to update, otherwise I jump to the new landmark case
#display(number_of_known_landmarks);
  	if ((number_of_known_landmarks > 0))
		%observation noise
		noise = 0.01;
		sigma_z = eye(3*number_of_known_landmarks)*noise;
#		display("size of sigma_z, C_t & sigma");
#		display(size(sigma_z));
#		display(size(C_t));
#		display(size(sigma));		
		%Kalman gain
		K = sigma*transpose(C_t)*(inv(C_t*sigma*transpose(C_t) + sigma_z));
		%update mu
		error = (z_t - h_t);
		correction = K*error;
#		display(error);
#		display(K);
#		display(correction);
#		display(mu);
		mu = mu + correction;
#		display(mu);
		%update sigma
		sigma = (eye(dimension_state) - K*C_t)*sigma;		
	endif
display("correction is done!");
end
