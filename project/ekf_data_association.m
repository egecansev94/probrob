close all
clear
clc

addpath '../'
addpath '../tools/g2o_wrapper'
addpath '../tools/visualization'
source '../tools/utilities/geometry_helpers_2d.m'

[landmarks, poses, transitions, observations, offsets, no_obs_id] = loadG2o_project('kalman_based_imu_slam.g2o');
display(length(observations));
display(length(transitions));

%% init stuff
%initial pose
%%mu = rand(3,1)*20-10;
%%mu(3) = normalizeAngle(mu(3));
%%printf('Random initial pose: [%f, %f, %f]\n', mu(1),mu(2), mu(3));
printf('\n***************** Press [SPACE] to perform a step *****************\n');
fflush(stdout);

%init covariance
mu = zeros(6,1);
sigma = zeros(6);

#bookkeeping: to and from mapping between robot pose (x,y, theta) and landmark indices (i)
#all mappings are initialized with invalid value -1 (meaning that the index is not mapped)
#since we do not know how many landmarks we will observe, we allocate a large enough buffer
id_to_state_map = ones(10000, 1)*-1;
state_to_id_map = ones(10000, 1)*-1;

#initialize data association
last_landmark_id = 1;

#------------------------------------------ VISUALIZATION ONLY ------------------------------------------
#initialize GUI with initial situation
frame = input('Please select the coordinate frame: xy / xz: ', 's');
figure("name", "ekf_slam",    #figure title
       "numbertitle", "on"); #remove figure number

#figure("name", "ekf_slam_dublicated",    #figure title
#       "numbertitle", "on"); #remove figure number
#pause();
if frame =='xy'
trajectory_xy = [transitions(1).v(1), transitions(1).v(2)];
#display(size(trajectory));
real_trajectory_xy = [poses(1).x, poses(1).y];
#display(size(real_trajectory));
elseif frame=='xz'
trajectory_xz = [transitions(1).v(1), transitions(1).v(3)];
#display(size(trajectory));
real_trajectory_xz = [poses(1).x, poses(1).z];
#display(size(real_trajectory));
endif
#------------------------------------------ VISUALIZATION ONLY

#figure(2); title("expected motion");
#xlim([-15, 5]);
#ylim([-15, 5]);

#for i = 2:length(poses)
#	plot(real_trajectory);
#	display(poses(i).x);
#	display(poses(i).y);
#	real_trajectory = [real_trajectory; poses(i).x, poses(i).y];
#	pause;
#	hold off;
#end
j = 1;

%simulation cycle
for i = 1:length(transitions)

	%predict
	[mu, sigma] = prediction(mu, sigma, transitions(i));
	%display(observations(j).observation);
display(size(sigma));
	if !any (no_obs_id == transitions(i).id_from)
		
		#obtain current observation using the data association
		observations_t = associateLandmarkIDs_project(mu, sigma, observations(j), state_to_id_map, offsets(1));

#		#EKF correct
#		[mu, sigma, id_to_state_map, state_to_id_map] = correction(mu, sigma, observations(j), offsets(1), id_to_state_map, state_to_id_map);
#display(size(sigma));
#		#ADD new landmarks to the staste
#  		[mu, sigma, id_to_state_map, state_to_id_map] = addNewLandmarks(mu, sigma, observations(j), offsets(1), id_to_state_map, state_to_id_map);
display('compare mu values:');
display(mu);
		#EKF correct
		[mu, sigma, id_to_state_map, state_to_id_map] = correction(mu, sigma, observations_t, offsets(1), id_to_state_map, state_to_id_map);
display(mu);
		#ADD new landmarks to the staste
  		[mu, sigma, id_to_state_map, state_to_id_map] = addNewLandmarks(mu, sigma, observations_t, offsets(1), id_to_state_map, state_to_id_map, last_landmark_id);


display(size(sigma));
	end
	%display("in main display:");
%display(mu);
	#------------------------------------------ VISUALIZATION ONLY ------------------------------------------
  #display current state - transform data
  %N = (rows(mu)-3)/2;
%display(rows(mu));
  N = (rows(mu)-6)/3;
%display(N);
  if N > 0
#    landmarks = landmark(state_to_id_map(1), [mu(4), mu(5)]);
#    for u = 2:N
#      landmarks(end+1) = landmark(state_to_id_map(u), [mu(3+2*u-1), mu(3+2*u)]);
#    endfor
    landmarks = landmark(state_to_id_map(1), [mu(7), mu(8), mu(9)]);
	%display(size(landmarks));
    for u = 2:N
      landmarks(end+1) = landmark(state_to_id_map(u), [mu(6+3*u-2), mu(6+3*u-1), mu(6+3*u)]);
	endfor
	%display(size(landmarks));

if frame == 'xy'
    printf("current pose: [%f, %f, %f], map size (landmarks): %u\n", mu(1), mu(2), mu(6), N);
    trajectory_xy = [trajectory_xy; mu(1), mu(2)];
    real_trajectory_xy = [real_trajectory_xy; poses(i).x, poses(i).y];
    plotState_xy(landmarks, mu, sigma, observations_t, trajectory_xy, real_trajectory_xy, offsets(1));

elseif frame == 'xz'
printf("current pose: [%f, %f, %f], map size (landmarks): %u\n", mu(1), mu(3), mu(5), N);
	figure(1);
	trajectory_xz = [trajectory_xz; mu(1), mu(3)];
	real_trajectory_xz = [real_trajectory_xz; poses(i).x, poses(i).z];
	plotState_xz(landmarks, mu, sigma, observations(j), trajectory_xz, real_trajectory_xz, offsets(1));
endif
#display(trajectory_xz);
#display(real_trajectory_xz);

#figure(2);
#	
#	printf("current pose: [%f, %f, %f], map size (landmarks): %u\n", mu(1), mu(2), mu(6), N);
#    	trajectory_xz = [trajectory; mu(1), mu(3)];

#    real_trajectory_xz = [real_trajectory; poses(i).x, poses(i).z];
#	trajectory = trajectory_xz;
#	real_trajectory = real_trajectory_xz;
#    plotState_xz(landmarks, mu, sigma, observations(j), trajectory, real_trajectory);
  endif
 % pause();
  fflush(stdout);	
#------------------------------------------ VISUALIZATION ONLY ------------------------------------------

	if !any (no_obs_id == transitions(i).id_from)
		j = j + 1;
	end
display("one more red nightmare");
pause();

endfor
display('alizvel');
pause();
