# retrieve the data associations
# 
# inputs:
#   mu: current mean
#   sigma: current covariance
#   observations: current observation set, from G2oWrapper
#   state_to_id_map: mapping vector state_position-id
#   heuristics: may be used to input the desired level of heuristics
#
# outputs:
#   observations: with the respective, aka associated, ids

function observations = associateLandmarkIDs_project(mu, sigma, observations, state_to_id_map, offset)
	display('Landmark association started..');
	display(observations.observation);
	measurements = observations.observation;

	#determine how many landmarks we have seen in this step
	M = length(measurements);


	mu_x = mu(1);
	mu_y = mu(2);
	mu_z = mu(3);
	mu_phi = mu(4);
	mu_theta = mu(5);
	mu_psi = mu(6);

	mu_t = mu(1:3,:);
	mu_r = mu(4:6,:);	

	%Body 3-2-1
	Rx = [1 0 0; 0 cos(mu_phi) -sin(mu_phi); 0 sin(mu_phi) cos(mu_phi)];
	Ry = [cos(mu_theta) 0 sin(mu_theta); 0 1 0; -sin(mu_theta) 0 cos(mu_theta)];
	Rz = [cos(mu_psi) -sin(mu_psi) 0; sin(mu_psi) cos(mu_psi) 0; 0 0 1];
	dRx = Rz*Ry*[0 0 0; 0 -sin(mu_phi) -cos(mu_phi); 0 cos(mu_phi) -sin(mu_phi)];
	dRy = Rz*[-sin(mu_theta) 0 cos(mu_theta); 0 0 0; -cos(mu_theta) 0 -sin(mu_theta)]*Rx;
	dRz = [-sin(mu_psi) -cos(mu_psi) 0; cos(mu_psi) -sin(mu_psi) 0; 0 0 0]*Ry*Rx;
	R = Rz*Ry*Rx;	

	# sensor offset
	offset_x = offset.x;
	offset_y = offset.y;
	offset_z = offset.z;
	offset_phi = offset.phi;
	offset_theta = offset.theta;
	offset_psi = offset.psi;

	%Body 3-2-1
	Rsx = [1 0 0; 0 cos(offset_phi) -sin(offset_phi); 0 sin(offset_phi) cos(offset_phi)];
	Rsy = [cos(offset_theta) 0 sin(offset_theta); 0 1 0; -sin(offset_theta) 0 cos(offset_theta)];
	Rsz = [cos(offset_psi) -sin(offset_psi) 0; sin(offset_psi) cos(offset_psi) 0; 0 0 1];
	Rs = Rsz*Rsy*Rsx;
	%Rs = Rsx*Rsy*Rsz;
	sens_xyz = [offset_x; offset_y; offset_z];	


	#set all the observations to unknown
	for i=1:M
		observations.observation(i).id = -1;
	endfor

	# dimension of the state (robot pose + landmark positions)
	state_dim = size(mu, 1);

	# dimension of the current map (how many landmarks we have already seen)
	N = (state_dim-6)/3;
 	
#	display(N);
#	display(M);
#	display(size(mu));
	#if we do not any landmarks in the state to associate or have no measurements 
	if(N == 0 || M == 0)
		return;
	endif

	#readability
	mu_landmarks = mu(7:end);
	mu_robot     = mu(1:6);
	#display(mu_landmarks);
	#build the association matrix [(current observations) x (number of landmarks)] - with maximum distance values
	A = ones(M, N)*1e3;
	A_new = ones(M, N)*1e3;
  #state iterator (we will use this to iterate over the landmarks in the state)
  id_state = 1;

	#now we have to populate the association matrix
	for n=1:N

		#extract landmark from mu
		mu_curr_landmark = mu_landmarks(id_state:id_state+2);


		#compute measurement function
		[h, C] = measurement_function_project(state_dim, mu_robot, mu_curr_landmark, id_state);
	
	
		sigma_zx = eye(2,2)*0.01;
		
		sigma_nn = C*sigma*C'+sigma_zx;
		
    #compute information matrix
		omega_nn = inv(sigma_nn);
		#display(omega_nn);
    #for all landmarks
		for m=1:M

			#obtain current measurement
      measurement = measurements(m);
theta = offset.psi;
			c=cos(theta);
	s=sin(theta);

	Rt=[c,-s;s c];
			z = [measurement.x_pose; measurement.y_pose; measurement.z_pose];
			meas = Rs * z + sens_xyz;   #Rs
			abs_meas = mu_t + R*meas;
			#abs_meas_2d = Rt * z(1:2) + sens_xyz(1:2);
			#display(h);
			#display(z);
			#display(abs_meas);
			#display(meas);
			#display(omega_nn);
#			display(abs_meas_2d);
			#pause();
			#compute likelihood for this measurement and landmark

			#A(m,n) = (z(1:2) - h)' * omega_nn * (z(1:2) - h);
			omega_nn = eye(2);
			#A_new(m,n) = (abs_meas(1:2) - h)' * omega_nn * (abs_meas(1:2) - h);
			A(m,n) = (meas(1:2) - h)' * omega_nn * (meas(1:2) - h);
		endfor

    #move to the next landmark in the state
		id_state += 3;
	endfor
#display(A);
#display(A_new);
#display(A_new_new);
display("association matrix is done!");
#pause();
	#now associate the measurement to the most promising landmark
	# proposed associations will be stored in a [Mx3] matrix composed
	# in this way
	#
	#	[measurement id, proposed landmark id , association matrix value a_mn] 	
	#
	# we will populate such a matrix with the associations surviving
	# the gating heuristic for each step
	# 
	# In the best friends and lonely best friend heuristics we will keep
	# the association as is in case of success, otherwise we will put
	# an id=0 to that measurment, meaning that the association is doubtful

	#configuration heuristics
	gating_tau                         = 1000;
  lonely_best_friend_gamma_threshold = 1e-3;

	#1. Gating
  for m=1:M

		#return the min and index on the 'm-th' row
		[a_mn, min_index] = min(A(m,:));
		#display(a_mn);
    #if the association passes the gate
		if(a_mn < gating_tau)	
		
			#add the possible association - creating the associations vector
      #[measurement id, proposed landmark id , association matrix value a_mn] 
			associations(end+1,:) = [m, min_index, a_mn];
		endif
  endfor

	#associations that survived the gating
	number_of_gated_associations = size(associations, 1);

#	#2. Best friends
#	for i=1:number_of_gated_associations
#		a_mn                 = associations(i, 3);
#		proposed_landmark_id = associations(i, 2);

#    #compute column minimum
#		min_on_column = min(A(:, proposed_landmark_id));
#		display(a_mn);
#		display(min_on_column);
#    #if the association is not the minimum in the column
#		if(a_mn != min_on_column)
#			associations(i, 2) = 0; #discard association, it is doubtful
#		endif
#	endfor
#	
#	#3. Lonely best friend
#  number_of_valid_associations = 0;
#	if(M > 1)
#		display(associations);
#		for i=1:number_of_gated_associations
#			a_mn                 = associations(i, 3);
#			measurement_id       = associations(i, 1);
#			proposed_landmark_id = associations(i, 2);

#      #this association is doubtful, skip evaluation
#			if(proposed_landmark_id == 0)
#				continue;
#			endif

#			#obtain second best(aka min) value of the row
#			ordered_row          = unique(A(measurement_id,:));
#			second_row_min_value = ordered_row(2);

#			#obtain second best(aka min) value of the column
#			ordered_col          = unique(A(:,proposed_landmark_id));
#			second_col_min_value = ordered_col(2);

#			#check if the association is ambiguous
#			if( (second_row_min_value - a_mn) < lonely_best_friend_gamma_threshold ||
#		 	    (second_col_min_value - a_mn) < lonely_best_friend_gamma_threshold )

#        #discard association, it is doubtful
#				associations(i,2) = 0;
#			else
#        
#        #we have found a valid association!
#        ++number_of_valid_associations;
#      endif
#		endfor
#	endif

	#assign the associations to the observations
	for i=1:number_of_gated_associations
		measurement_id       = associations(i, 1);
		proposed_landmark_id = associations(i, 2);		

		##assign the proposed association OR 0 (a magic number that indicates no landmark was found)
		observations.observation(measurement_id).id = proposed_landmark_id;
	endfor

  #info
  %printf("valid associations: %u / measurements: %u / landmarks: %u\n", number_of_valid_associations, M, N);
  display('Landmark association is done!');
end
